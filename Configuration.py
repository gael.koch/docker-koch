"""
simple configuration file. Enable the user to modify the program by changing parameters
"""

DATA_RNOY = "rnoy_"
DATA_BNOY = "bnoy_"
DATA_ABOB = "abob_"
DATA_BBOB = "bbob_"
DATA_MUR = "mur_"
DATA_INDUCTANCE = "inductance1"
DATA_INDUCTANCE_LOG = "inductance1Log"
DATA_SURFACE_NOY = "surfaceNoy_"
DATA_SURFACE_BOB = "surfaceBob_"
PATH_DATA = "data/Inducylrout_105000_pts.csv"
MODEL_TEST_SIZE = 0.33
PROGRESSIVE_TEST_SIZE = 0.1
PROGRESSIVE_TESTING_LIMIT = 21000 #x*2 each iteratin
