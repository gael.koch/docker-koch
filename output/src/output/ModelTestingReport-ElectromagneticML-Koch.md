
sklearn report model testing
============================

Contents
========

* [Introduction](#introduction)
* [linear regression](#linear-regression)
	* [Scatter des prédictions](#scatter-des-prdictions)
	* [Erreur des prédictions](#erreur-des-prdictions)
	* [Scoring du modèle](#scoring-du-modle)
* [gradient boosting](#gradient-boosting)
	* [Scatter des prédictions](#scatter-des-prdictions)
	* [Erreur des prédictions](#erreur-des-prdictions)
	* [Scoring du modèle](#scoring-du-modle)
* [scaled linear regression](#scaled-linear-regression)
	* [Scatter des prédictions](#scatter-des-prdictions)
	* [Erreur des prédictions](#erreur-des-prdictions)
	* [Scoring du modèle](#scoring-du-modle)
* [scaled gradient boosting](#scaled-gradient-boosting)
	* [Scatter des prédictions](#scatter-des-prdictions)
	* [Erreur des prédictions](#erreur-des-prdictions)
	* [Scoring du modèle](#scoring-du-modle)
* [logarithmic linear regression](#logarithmic-linear-regression)
	* [Scatter des prédictions](#scatter-des-prdictions)
	* [Erreur des prédictions](#erreur-des-prdictions)
	* [Scoring du modèle](#scoring-du-modle)
* [logarithmic gradient boosting](#logarithmic-gradient-boosting)
	* [Scatter des prédictions](#scatter-des-prdictions)
	* [Erreur des prédictions](#erreur-des-prdictions)
	* [Scoring du modèle](#scoring-du-modle)
* [logarithmic scaled linear regression](#logarithmic-scaled-linear-regression)
	* [Scatter des prédictions](#scatter-des-prdictions)
	* [Erreur des prédictions](#erreur-des-prdictions)
	* [Scoring du modèle](#scoring-du-modle)
* [logarithmic scaled gradient boosting](#logarithmic-scaled-gradient-boosting)
	* [Scatter des prédictions](#scatter-des-prdictions)
	* [Erreur des prédictions](#erreur-des-prdictions)
	* [Scoring du modèle](#scoring-du-modle)
* [logarithmic  +surface linear regression](#logarithmic--surface-linear-regression)
	* [Scatter des prédictions](#scatter-des-prdictions)
	* [Erreur des prédictions](#erreur-des-prdictions)
	* [Scoring du modèle](#scoring-du-modle)
* [logarithmic  +surface gradient boosting](#logarithmic--surface-gradient-boosting)
	* [Scatter des prédictions](#scatter-des-prdictions)
	* [Erreur des prédictions](#erreur-des-prdictions)
	* [Scoring du modèle](#scoring-du-modle)
* [logarithmic  +surface scaled linear regression](#logarithmic--surface-scaled-linear-regression)
	* [Scatter des prédictions](#scatter-des-prdictions)
	* [Erreur des prédictions](#erreur-des-prdictions)
	* [Scoring du modèle](#scoring-du-modle)
* [logarithmic  +surface scaled gradient boosting](#logarithmic--surface-scaled-gradient-boosting)
	* [Scatter des prédictions](#scatter-des-prdictions)
	* [Erreur des prédictions](#erreur-des-prdictions)
	* [Scoring du modèle](#scoring-du-modle)

# Introduction


Ce fichier contient tous les résultats du test des modèles de machine learning de sklearn
# linear regression

## Scatter des prédictions
  
![vue d'ensemble des prédictions par rapport aux vraies valeurs](images/1-scatterBig.png)  
![Zoom sur les petites valeurs des prédictions par rapport aux vraies valeurs](images/1-scatterSmall.png)
## Erreur des prédictions
  
![Erreur absolue des prédictions par rapport aux vrai valeurs. Trié selon l'inductance du plus petit au plus 
grand](images/1-errorAbsolute.png)  
![Erreur absolue relative des prédictions par rapport aux vrai valeurs. Trié selon l'inductance du plus petit au plus 
grand](images/1-errorRelative.png)
## Scoring du modèle

|Métrique|Valeur|
| :---: | :---: |
|R^2|0.843|
|Min erreur absolue : |5.277e-12|
|Max erreur absolue : |3.778e-06|
|Moyenne erreur absolue : |2.193e-07|
|Min erreur absolue relative : |5.434e-04%|
|Max erreur absolue relative : |502221.437%|
|Moyenne erreur absolue relative : |558.242%|

# gradient boosting

## Scatter des prédictions
  
![vue d'ensemble des prédictions par rapport aux vraies valeurs](images/2-scatterBig.png)  
![Zoom sur les petites valeurs des prédictions par rapport aux vraies valeurs](images/2-scatterSmall.png)
## Erreur des prédictions
  
![Erreur absolue des prédictions par rapport aux vrai valeurs. Trié selon l'inductance du plus petit au plus 
grand](images/2-errorAbsolute.png)  
![Erreur absolue relative des prédictions par rapport aux vrai valeurs. Trié selon l'inductance du plus petit au plus 
grand](images/2-errorRelative.png)
## Scoring du modèle

|Métrique|Valeur|
| :---: | :---: |
|R^2|0.991|
|Min erreur absolue : |2.059e-12|
|Max erreur absolue : |2.115e-06|
|Moyenne erreur absolue : |4.949e-08|
|Min erreur absolue relative : |1.584e-04%|
|Max erreur absolue relative : |46335.125%|
|Moyenne erreur absolue relative : |78.881%|

# scaled linear regression

## Scatter des prédictions
  
![vue d'ensemble des prédictions par rapport aux vraies valeurs](images/3-scatterBig.png)  
![Zoom sur les petites valeurs des prédictions par rapport aux vraies valeurs](images/3-scatterSmall.png)
## Erreur des prédictions
  
![Erreur absolue des prédictions par rapport aux vrai valeurs. Trié selon l'inductance du plus petit au plus 
grand](images/3-errorAbsolute.png)  
![Erreur absolue relative des prédictions par rapport aux vrai valeurs. Trié selon l'inductance du plus petit au plus 
grand](images/3-errorRelative.png)
## Scoring du modèle

|Métrique|Valeur|
| :---: | :---: |
|R^2|0.841|
|Min erreur absolue : |5.689e-12|
|Max erreur absolue : |4.017e-06|
|Moyenne erreur absolue : |2.201e-07|
|Min erreur absolue relative : |5.793e-04%|
|Max erreur absolue relative : |727568.795%|
|Moyenne erreur absolue relative : |620.501%|

# scaled gradient boosting

## Scatter des prédictions
  
![vue d'ensemble des prédictions par rapport aux vraies valeurs](images/4-scatterBig.png)  
![Zoom sur les petites valeurs des prédictions par rapport aux vraies valeurs](images/4-scatterSmall.png)
## Erreur des prédictions
  
![Erreur absolue des prédictions par rapport aux vrai valeurs. Trié selon l'inductance du plus petit au plus 
grand](images/4-errorAbsolute.png)  
![Erreur absolue relative des prédictions par rapport aux vrai valeurs. Trié selon l'inductance du plus petit au plus 
grand](images/4-errorRelative.png)
## Scoring du modèle

|Métrique|Valeur|
| :---: | :---: |
|R^2|0.99|
|Min erreur absolue : |1.908e-12|
|Max erreur absolue : |2.500e-06|
|Moyenne erreur absolue : |5.074e-08|
|Min erreur absolue relative : |3.256e-04%|
|Max erreur absolue relative : |86933.653%|
|Moyenne erreur absolue relative : |101.398%|

# logarithmic linear regression

## Scatter des prédictions
  
![vue d'ensemble des prédictions par rapport aux vraies valeurs](images/5-scatterBig.png)  
![Zoom sur les petites valeurs des prédictions par rapport aux vraies valeurs](images/5-scatterSmall.png)
## Erreur des prédictions
  
![Erreur absolue des prédictions par rapport aux vrai valeurs. Trié selon l'inductance du plus petit au plus 
grand](images/5-errorAbsolute.png)  
![Erreur absolue relative des prédictions par rapport aux vrai valeurs. Trié selon l'inductance du plus petit au plus 
grand](images/5-errorRelative.png)
## Scoring du modèle

|Métrique|Valeur|
| :---: | :---: |
|R^2|0.839|
|Min erreur absolue : |6.351e-13|
|Max erreur absolue : |1.240e-05|
|Moyenne erreur absolue : |3.948e-07|
|Min erreur absolue relative : |0.001%|
|Max erreur absolue relative : |15348.752%|
|Moyenne erreur absolue relative : |59.879%|

# logarithmic gradient boosting

## Scatter des prédictions
  
![vue d'ensemble des prédictions par rapport aux vraies valeurs](images/6-scatterBig.png)  
![Zoom sur les petites valeurs des prédictions par rapport aux vraies valeurs](images/6-scatterSmall.png)
## Erreur des prédictions
  
![Erreur absolue des prédictions par rapport aux vrai valeurs. Trié selon l'inductance du plus petit au plus 
grand](images/6-errorAbsolute.png)  
![Erreur absolue relative des prédictions par rapport aux vrai valeurs. Trié selon l'inductance du plus petit au plus 
grand](images/6-errorRelative.png)
## Scoring du modèle

|Métrique|Valeur|
| :---: | :---: |
|R^2|0.996|
|Min erreur absolue : |7.675e-13|
|Max erreur absolue : |2.538e-06|
|Moyenne erreur absolue : |4.267e-08|
|Min erreur absolue relative : |5.713e-05%|
|Max erreur absolue relative : |265.099%|
|Moyenne erreur absolue relative : |5.838%|

# logarithmic scaled linear regression

## Scatter des prédictions
  
![vue d'ensemble des prédictions par rapport aux vraies valeurs](images/7-scatterBig.png)  
![Zoom sur les petites valeurs des prédictions par rapport aux vraies valeurs](images/7-scatterSmall.png)
## Erreur des prédictions
  
![Erreur absolue des prédictions par rapport aux vrai valeurs. Trié selon l'inductance du plus petit au plus 
grand](images/7-errorAbsolute.png)  
![Erreur absolue relative des prédictions par rapport aux vrai valeurs. Trié selon l'inductance du plus petit au plus 
grand](images/7-errorRelative.png)
## Scoring du modèle

|Métrique|Valeur|
| :---: | :---: |
|R^2|0.842|
|Min erreur absolue : |2.265e-12|
|Max erreur absolue : |1.231e-05|
|Moyenne erreur absolue : |3.830e-07|
|Min erreur absolue relative : |0.003%|
|Max erreur absolue relative : |14385.947%|
|Moyenne erreur absolue relative : |58.425%|

# logarithmic scaled gradient boosting

## Scatter des prédictions
  
![vue d'ensemble des prédictions par rapport aux vraies valeurs](images/8-scatterBig.png)  
![Zoom sur les petites valeurs des prédictions par rapport aux vraies valeurs](images/8-scatterSmall.png)
## Erreur des prédictions
  
![Erreur absolue des prédictions par rapport aux vrai valeurs. Trié selon l'inductance du plus petit au plus 
grand](images/8-errorAbsolute.png)  
![Erreur absolue relative des prédictions par rapport aux vrai valeurs. Trié selon l'inductance du plus petit au plus 
grand](images/8-errorRelative.png)
## Scoring du modèle

|Métrique|Valeur|
| :---: | :---: |
|R^2|0.996|
|Min erreur absolue : |8.116e-13|
|Max erreur absolue : |3.118e-06|
|Moyenne erreur absolue : |4.326e-08|
|Min erreur absolue relative : |1.134e-04%|
|Max erreur absolue relative : |253.264%|
|Moyenne erreur absolue relative : |5.696%|

# logarithmic  +surface linear regression

## Scatter des prédictions
  
![vue d'ensemble des prédictions par rapport aux vraies valeurs](images/9-scatterBig.png)  
![Zoom sur les petites valeurs des prédictions par rapport aux vraies valeurs](images/9-scatterSmall.png)
## Erreur des prédictions
  
![Erreur absolue des prédictions par rapport aux vrai valeurs. Trié selon l'inductance du plus petit au plus 
grand](images/9-errorAbsolute.png)  
![Erreur absolue relative des prédictions par rapport aux vrai valeurs. Trié selon l'inductance du plus petit au plus 
grand](images/9-errorRelative.png)
## Scoring du modèle

|Métrique|Valeur|
| :---: | :---: |
|R^2|0.84|
|Min erreur absolue : |1.158e-11|
|Max erreur absolue : |1.232e-05|
|Moyenne erreur absolue : |3.831e-07|
|Min erreur absolue relative : |0.001%|
|Max erreur absolue relative : |14109.616%|
|Moyenne erreur absolue relative : |58.808%|

# logarithmic  +surface gradient boosting

## Scatter des prédictions
  
![vue d'ensemble des prédictions par rapport aux vraies valeurs](images/10-scatterBig.png)  
![Zoom sur les petites valeurs des prédictions par rapport aux vraies valeurs](images/10-scatterSmall.png)
## Erreur des prédictions
  
![Erreur absolue des prédictions par rapport aux vrai valeurs. Trié selon l'inductance du plus petit au plus 
grand](images/10-errorAbsolute.png)  
![Erreur absolue relative des prédictions par rapport aux vrai valeurs. Trié selon l'inductance du plus petit au plus 
grand](images/10-errorRelative.png)
## Scoring du modèle

|Métrique|Valeur|
| :---: | :---: |
|R^2|0.996|
|Min erreur absolue : |9.265e-13|
|Max erreur absolue : |2.824e-06|
|Moyenne erreur absolue : |4.483e-08|
|Min erreur absolue relative : |3.619e-04%|
|Max erreur absolue relative : |239.3%|
|Moyenne erreur absolue relative : |5.968%|

# logarithmic  +surface scaled linear regression

## Scatter des prédictions
  
![vue d'ensemble des prédictions par rapport aux vraies valeurs](images/11-scatterBig.png)  
![Zoom sur les petites valeurs des prédictions par rapport aux vraies valeurs](images/11-scatterSmall.png)
## Erreur des prédictions
  
![Erreur absolue des prédictions par rapport aux vrai valeurs. Trié selon l'inductance du plus petit au plus 
grand](images/11-errorAbsolute.png)  
![Erreur absolue relative des prédictions par rapport aux vrai valeurs. Trié selon l'inductance du plus petit au plus 
grand](images/11-errorRelative.png)
## Scoring du modèle

|Métrique|Valeur|
| :---: | :---: |
|R^2|0.839|
|Min erreur absolue : |3.301e-12|
|Max erreur absolue : |1.221e-05|
|Moyenne erreur absolue : |3.831e-07|
|Min erreur absolue relative : |7.841e-04%|
|Max erreur absolue relative : |15546.332%|
|Moyenne erreur absolue relative : |60.539%|

# logarithmic  +surface scaled gradient boosting

## Scatter des prédictions
  
![vue d'ensemble des prédictions par rapport aux vraies valeurs](images/12-scatterBig.png)  
![Zoom sur les petites valeurs des prédictions par rapport aux vraies valeurs](images/12-scatterSmall.png)
## Erreur des prédictions
  
![Erreur absolue des prédictions par rapport aux vrai valeurs. Trié selon l'inductance du plus petit au plus 
grand](images/12-errorAbsolute.png)  
![Erreur absolue relative des prédictions par rapport aux vrai valeurs. Trié selon l'inductance du plus petit au plus 
grand](images/12-errorRelative.png)
## Scoring du modèle

|Métrique|Valeur|
| :---: | :---: |
|R^2|0.996|
|Min erreur absolue : |2.149e-14|
|Max erreur absolue : |2.532e-06|
|Moyenne erreur absolue : |4.699e-08|
|Min erreur absolue relative : |4.851e-06%|
|Max erreur absolue relative : |162.325%|
|Moyenne erreur absolue relative : |6.176%|

# Resume

|Nom modèle|R^2|Min erreur absolue|Max erreur absolue|Moyenne absolue|Min erreur absolue relative|Max erreur absolue relative|Moyenne erreur absolue relative|
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
|linear regression|0.843|5.277e-12|3.778e-06|2.193e-07|5.434e-04%|502221.437%|558.242%|
|gradient boosting|0.991|2.059e-12|2.115e-06|4.949e-08|1.584e-04%|46335.125%|78.881%|
|scaled linear regression|0.841|5.689e-12|4.017e-06|2.201e-07|5.793e-04%|727568.795%|620.501%|
|scaled gradient boosting|0.99|1.908e-12|2.500e-06|5.074e-08|3.256e-04%|86933.653%|101.398%|
|logarithmic linear regression|0.839|6.351e-13|1.240e-05|3.948e-07|0.001%|15348.752%|59.879%|
|logarithmic gradient boosting|0.996|7.675e-13|2.538e-06|4.267e-08|5.713e-05%|265.099%|5.838%|
|logarithmic scaled linear regression|0.842|2.265e-12|1.231e-05|3.830e-07|0.003%|14385.947%|58.425%|
|logarithmic scaled gradient boosting|0.996|8.116e-13|3.118e-06|4.326e-08|1.134e-04%|253.264%|5.696%|
|logarithmic  +surface linear regression|0.84|1.158e-11|1.232e-05|3.831e-07|0.001%|14109.616%|58.808%|
|logarithmic  +surface gradient boosting|0.996|9.265e-13|2.824e-06|4.483e-08|3.619e-04%|239.3%|5.968%|
|logarithmic  +surface scaled linear regression|0.839|3.301e-12|1.221e-05|3.831e-07|7.841e-04%|15546.332%|60.539%|
|logarithmic  +surface scaled gradient boosting|0.996|2.149e-14|2.532e-06|4.699e-08|4.851e-06%|162.325%|6.176%|
