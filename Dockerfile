FROM ubuntu as builder

RUN mkdir /src

ADD . /src

WORKDIR /src

# Install python/pip
RUN mkdir -p data && mkdir -p output && chmod -R 777 data && chmod -R 777 output && apt-get update -y && apt-get install -y python && apt-get install -y pip && pip install -r requirements.txt && pip install pyinstaller && pyinstaller --onefile modelTesting.py --hidden-import sklearn.utils._typedefs --hidden-import sklearn.neighbors._partition_nodes

FROM ubuntu

WORKDIR /src

RUN apt-get update -y && apt-get install -y python

COPY --from=builder /src/dist/modelTesting /src

RUN mkdir -p data && mkdir -p output/images && chmod -R 777 data && chmod -R 777 output

COPY Inducylrout_105000_pts.csv /src/data

CMD mkdir -p output/images && ./modelTesting
