import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
import Configuration as conf

class DataFormatter:
    """
    handles the generation of the data to train models.
    Can scale data, generate the log of the inductance and add surface to height ratio to the data
    """
    scaler = StandardScaler()

    def prepareData(self, df):
        """
        convert the data into the right type. In this case float64
        :param df: the datas
        :return: a new data with the correct types
        """
        df[conf.DATA_RNOY] = df[conf.DATA_RNOY].astype('float64')
        df[conf.DATA_BNOY] = df[conf.DATA_BNOY].astype('float64')
        df[conf.DATA_ABOB] = df[conf.DATA_ABOB].astype('float64')
        df[conf.DATA_BBOB] = df[conf.DATA_BBOB].astype('float64')
        df[conf.DATA_MUR] = df[conf.DATA_MUR].astype('int64')
        try:
            df[conf.DATA_INDUCTANCE] = df[conf.DATA_INDUCTANCE].astype('float64')
        except:
            print() #Do nothing because it is just not present in the set
        try:
            df[conf.DATA_INDUCTANCE_LOG] = df[conf.DATA_INDUCTANCE_LOG].astype('float64')
        except:
            print()  # Do nothing because it is just not present in the set
        return df

    def getStandardSets(self, test_size):
        """
        get the data without any modification
        :param test_size: the size of the testing set, should be between [0-1]
        :return: the training / testing data sets
        """
        # get data
        df = pd.read_csv(conf.PATH_DATA)
        df = self.prepareData(df)

        # prepare the training data and the data to test the model
        x = np.asarray(df[[conf.DATA_RNOY, conf.DATA_BNOY, conf.DATA_ABOB, conf.DATA_BBOB, conf.DATA_MUR]])
        y = np.asarray(df[conf.DATA_INDUCTANCE])
        return train_test_split(x, y, test_size=test_size, shuffle=True)

    def getStandardScaledSets(self, test_size):
        """
        get the data with scaling on the features
        :param test_size: the size of the testing set, should be between [0-1]
        :return: the training / testing data sets
        """
        # get data
        df = pd.read_csv(conf.PATH_DATA)
        df = self.prepareData(df)
        scaled = self.scaler.fit_transform(df[[conf.DATA_RNOY, conf.DATA_BNOY, conf.DATA_ABOB, conf.DATA_BBOB, conf.DATA_MUR]])

        # prepare the training data and the data to test the model
        x = np.asarray(scaled)
        y = np.asarray(df[conf.DATA_INDUCTANCE])
        return train_test_split(x, y, test_size=test_size, shuffle=True)

    def getLogSets(self, test_size, addData):
        """
        get the data with the log of the inductance
        :param test_size: the size of the testing set, should be between [0-1]
        :return: the training / testing data sets
        """
        # get data
        df = pd.read_csv(conf.PATH_DATA)
        df[conf.DATA_INDUCTANCE_LOG] = np.log10(df[conf.DATA_INDUCTANCE])
        df.drop([conf.DATA_INDUCTANCE], axis=1, inplace=True)

        df = self.prepareData(df)
        if(addData):
            df = self.addSurfaceToData(df)

        # prepare the training data and the data to test the model
        x = np.asarray(df[[conf.DATA_RNOY, conf.DATA_BNOY, conf.DATA_ABOB, conf.DATA_BBOB, conf.DATA_MUR]])
        y = np.asarray(df[conf.DATA_INDUCTANCE_LOG])

        return train_test_split(x, y, test_size=test_size, shuffle=True)

    def getLogScaledSets(self, test_size, addData):
        """
        get the data with scaling on the features and the log of the inductance
        :param test_size: the size of the testing set, should be between [0-1]
        :return: the training / testing data sets
        """
        # get data
        df = pd.read_csv(conf.PATH_DATA)
        df[conf.DATA_INDUCTANCE_LOG] = np.log10(df[conf.DATA_INDUCTANCE])
        df.drop([conf.DATA_INDUCTANCE], axis=1, inplace=True)

        df = self.prepareData(df)
        if (addData):
            df = self.addSurfaceToData(df)

        scaled = self.scaler.fit_transform(df[[conf.DATA_RNOY, conf.DATA_BNOY, conf.DATA_ABOB, conf.DATA_BBOB, conf.DATA_MUR]])

        # prepare the training data and the data to test the model
        x = np.asarray(scaled)
        y = np.asarray(df[conf.DATA_INDUCTANCE_LOG])
        return train_test_split(x, y, test_size=test_size, shuffle=True)

    def addSurfaceToData(self, df):
        """
        get the data with the surface to height ration added to the data
        :param df: the original data to add into
        :return: the new data
        """
        df[conf.DATA_SURFACE_NOY] = np.pi*np.power(df[conf.DATA_RNOY], 2) / df[conf.DATA_BNOY]
        df[conf.DATA_SURFACE_NOY] = df[conf.DATA_SURFACE_NOY].astype('float64')
        df[conf.DATA_SURFACE_BOB] = np.pi * np.power(df[conf.DATA_ABOB], 2) / df[conf.DATA_BBOB]
        df[conf.DATA_SURFACE_BOB] = df[conf.DATA_SURFACE_BOB].astype('float64')
        return df

    def reverseLog(self, data):
        """
        convert a number back to it's original form (log10 --> x)
        :param data: the number to convert
        :return: the converted number
        """
        return np.power(10, data)
